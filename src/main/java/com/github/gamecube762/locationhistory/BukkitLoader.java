package com.github.gamecube762.locationhistory;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class BukkitLoader extends JavaPlugin implements Listener, Runnable {

    HashMap<UUID, Vector> lastPositions = new HashMap<>();
    FileConfiguration config;
    LoggingMethod locationLogger;

    @Override
    public void onLoad() {
        if (!this.getDataFolder().exists())
            this.saveResource("config.yml", false);

        config = getConfig();
        config.addDefault("loggingMethod", "sqlite");
        config.addDefault("database.host", "localhost");
        config.addDefault("database.database", "minecraft");
        config.addDefault("database.username", "root");
        config.addDefault("database.password", "password");
        config.addDefault("database.useSSL", true);

        switch (config.getString("loggingMethod").toLowerCase()) {
            default:
            case "sqlite": locationLogger = SQLLogging.SQLite(this, Paths.get(this.getDataFolder().toString(), "locations.db"));  break;
            case "mysql":  locationLogger = SQLLogging.MySQL(this, config.getString("database.host"), config.getString("database.database"), config.getString("database.username"), config.getString("database.password"), config.getBoolean("database.useSSL")); break;
            case "yml":    locationLogger = new YAMLLogging(this); break;
        }
    }

    @Override
    public void onEnable() {
        int interval = config.getInt("interval") * 20;
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, this, interval, interval);
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        try { locationLogger.close(); }
        catch (Exception e) { getLogger().log(Level.WARNING, "Failed to close logger: " + e.getMessage()); }
    }

    @Override
    public void run() {
        Collection<? extends Player> players = this.getServer().getOnlinePlayers();

        if (players.isEmpty()) return;

        final Collection<LocationStamp> stamps = players.stream().filter(this::hasMoved).map(LocationStamp::fromPlayer).collect(Collectors.toList());

        this.getServer().getScheduler().runTaskAsynchronously(this, () -> {
            stamps.forEach(locationLogger::log);
            try { locationLogger.commit(); }
            catch (Exception e) { this.getLogger().log(Level.WARNING, "Failed to save locations: " + e.getMessage()); }
        });
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent e) {
        lastPositions.remove(e.getPlayer().getUniqueId());
    }

    public boolean hasMoved(Player player) {
        Vector oldVector = lastPositions.get(player.getUniqueId());
        Vector newVector = player.getLocation().toVector();

        lastPositions.put(player.getUniqueId(), player.getLocation().toVector());

        return oldVector == null || !oldVector.equals(newVector);
    }

    public static class LocationStamp {
        long timestamp;
        UUID uuid;
        Location location;

        public LocationStamp(long timestamp, UUID uuid, Location location) {
            this.timestamp = timestamp;
            this.uuid = uuid;
            this.location = location;
        }

        static LocationStamp fromPlayer(Player player) {
            return new LocationStamp(System.currentTimeMillis(), player.getUniqueId(), player.getLocation());
        }

    }

    public static interface LoggingMethod extends Closeable {

        public void log(LocationStamp ls);

        public void commit() throws Exception;

    }

    public static class YAMLLogging implements LoggingMethod {
        // YML causes the whole dataset to be loaded in ram
        // This also means it's writing the entire growing dataset every run()
        // Over time this would theoretically create longer ticks and eat more ram.
        // We could Async it, but that would only fix tick-rate

        private Path logFile;
        private FileConfiguration log;

        YAMLLogging(Plugin plugin) {
            logFile = Paths.get(plugin.getDataFolder().toString(), "locations.db");
            log = YamlConfiguration.loadConfiguration(logFile.toFile());
        }

        @Override
        public void close() throws IOException {
            commit();
        }

        @Override
        public void log(LocationStamp ls) {
            log.set(String.format("locations.%s.%s", ls.uuid, ls.timestamp), ls.location);
        }

        @Override
        public void commit() throws IOException {
            log.save(logFile.toString());
        }

    }

    public static class SQLLogging implements LoggingMethod {

        private Connection connection = null;
        private Queue<LocationStamp> buffer = new LinkedList<>();
        private String connectURI, username = "", password = "";

        SQLLogging(Plugin plugin, String driver, String connectURI, String username, String password) {
            this.connectURI = connectURI;
            this.username = username;
            this.password = password;

            // Test connection and ensure table exists
            try { createTable(); }
            catch (SQLException e) {
                plugin.getLogger().log(Level.SEVERE, String.format("Create Table Failed: %s", e.getMessage()));
            }
        }

        public static SQLLogging SQLite(Plugin plugin, Path filePath) {
            return new SQLLogging(plugin, "sqlite", "jdbc:sqlite:" + filePath, "", "");
        }

        public static SQLLogging MySQL(Plugin plugin, String host, String database, String username, String password, Boolean useSSL) {
            return new SQLLogging(plugin, "mysql", String.format("jdbc:mysql://%s/%s?useSSL=%s", host, database, useSSL), username, password);
        }

        public void createTable() throws SQLException {
            try (Statement statement = getDBConnection().createStatement()) {
                statement.executeUpdate("CREATE TABLE IF NOT EXISTS locations (timestamp DATE, uuid TEXT, world TEXT, x REAL, y REAL, z REAL, pitch REAL, yaw REAL)");
            }
        }

        public Connection getDBConnection() throws SQLException {
            return (connection != null && !connection.isClosed()) ? connection : (connection = DriverManager.getConnection(this.connectURI, this.username, this.password));
        }

        public void closeDB() throws SQLException {
            if (connection != null && !connection.isClosed())
                connection.close();
        }

        @Override
        public void close() throws IOException {
            try {
                commit();
                closeDB();
            } catch (Exception e) {
                throw new IOException(e);
            }
        }

        @Override
        public void log(LocationStamp ls) {
            buffer.add(ls);
        }

        @Override
        public void commit() throws Exception {
            if (buffer.isEmpty()) return;

            try (PreparedStatement statement = getDBConnection().prepareStatement("INSERT INTO locations (timestamp, uuid, world, x, y, z, pitch, yaw) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
                for (LocationStamp ls : buffer) {
                    statement.setDate(1, new Date(ls.timestamp));
                    statement.setString(2, ls.uuid.toString());
                    statement.setString(3, ls.location.getWorld().getName());
                    statement.setDouble(4, ls.location.getX());
                    statement.setDouble(5, ls.location.getY());
                    statement.setDouble(6, ls.location.getZ());
                    statement.setFloat(7, ls.location.getPitch());
                    statement.setFloat(8, ls.location.getYaw());
                    statement.execute();
                }
                buffer.clear();
            }
        }

    }

}
