LocationHistory
===

A simple plugin that records player's locations over time.

This plugin supports MC 1.8 to 1.16.5

Plugin Setup
---

1. Download the latest plugin build: [artifacts.zip](https://gitlab.com/Gamecube762/locationhistory/-/jobs/artifacts/master/download?job=build)

2. Copy the plugin jar file into your servers `/plugins` folder.

3. (Optional) Run the server once to generate the default config.

4. Edit `/plugins/LocationHistory/config.yml` to your liking.

Config.yml
--

``` YAML
# Logging method to use.
#
# Note:
#   SQLite is recommended over YML since YML keeps the growing dataset loaded in memory.
#   This will both eat up ram overtime and cause longer ticks each time it saves the file.
#
# Default: sqlite
loggingMethod: sqlite

# How often in seconds to store users' locations.
# Default: 30 seconds
interval: 30
```

Development Tips
---

VisualStudioCode:

* VSC build and debug tasks are included.

* Running the default build task (`crtl+shift+b`) will build the jar for you.

* Combined with running docker-compose, the default debug task(`F5`) will automatically attach to the running MC server.

* Hot-swapping live-code is setup when debugging.

Docker-Compose:

* This project has a simple docker-compose MC server setup. Simply run `docker-compose up` to start the server.

* `rcon.bat` and `rcon.sh` are pre-made scripts to rcon into the MC server.

* Debugging port `5005` is setup for your IDE's debugger to make use of.

* Docker can be installed from: <https://docs.docker.com/compose/install/>
